<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 3/7/19
 * Time: 8:56 PM
 */

namespace App\Traits;


use GuzzleHttp\Client;

trait ConsumesExternalServices
{
    public function performRequest($method, $requestUri, $formParams = [], $headers = [])
    {
        $client = new Client([ 'base_uri' => $this->baseUri ]);

        if(isset($this->secret)) {
            $headers['Authorization'] = $this->secret;
        }

        $params = [
            'form_params' => $formParams,
            'headers' => $headers,
        ];

        $response = $client->request($method, $requestUri, $params);

        return $response->getBody()->getContents();
    }
}
