<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 3/13/19
 * Time: 9:16 PM
 */

namespace App\Http\Middleware;


use Closure;

class Cors
{
    public function handle($request, Closure $next)
    {
        header("Access-Control-Allow-Origin: *");

        $headers = [
            'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Headers' => 'Content-Type, X-Auth-Token, Origin, Authorization, x-csrf-token, x-requested-with',
        ];

        if($request->getMethod() == 'OPTIONS') {
            return response()->json('ok', 200, $headers);
        }

        $response = $next($request);

        foreach($headers as $key => $value) {
            $response->header($key, $value);
        }

        return $response;
    }
}
